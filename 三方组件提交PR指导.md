# 三方组件提交PR指导

本指导书的目的是方便用户以最简单的方式将自己的三方组件信息合入到官方推广渠道[OpenHarmony-SIG/third_party_app_libs](https://gitee.com/openharmony-sig/third_party_app_libs)，方便其他广大OpenHarmony开发者了解和使用这些已完成的三方组件，避免重复造轮子。

## 一 PR提交前的准备

### 1.账号准备

用户提交Pull Request(下面简称PR)之前需要注册一个gitee账号，并设置好主邮箱。

注册地址：https://gitee.com/signup

邮箱设置界面：https://gitee.com/profile/emails



### 2.<a name="sign-account"></a>在DCO签署账号

账号注册完毕之后为了保证PR能合入，还需要签署DCO，签署网址：

https://dco.openharmony.io/sign/Z2l0ZWUlMkZvcGVuX2hhcm1vbnk=

签署完成之后需要查看gitee账号的签署状态，保证其状态为已签署。



### 3.三方组件信息合入准备工作

提交PR之前需要进行以下两个操作：

#### 3.1 <a name="code-prepare"></a>三方组件准备

需要准备在Openharmony开发板验证过且正常运行的三方组件。

需要使用到三方组件的信息包含：组件名称，组件url，组件介绍。

#### 3.2  fork仓库

由于我们的三方组件信息不可以在官方推广渠道的README文档里面直接添加，所以需要先fork这个仓库到我们的目标空间，然后在我们fork下来的third_party_app_libs仓库的README文档里面更新三方组件信息，最后通过PR的方式合入到官方推广渠道里面。 具体步骤为：

| 3.2.1 点击官方推广渠道仓库右上角的fork按钮，fork本库到自己的账号。 |
| ------------------------------------------------------------ |
| ![image-01](imgs/image-01.png)                               |
| 3.2.2 选择目标空间                                           |
| ![image-02](imgs/image-02.png)                               |
| 3.2.3 fork仓库完毕之后点击私人仓库右侧的同步按钮拉取源库的代码，保证两个仓的代码一致。 |
| ![image-03](imgs/image-03.png)                               |



#### 3.3<a name="get-info"></a> 获取提交信息和扩展信息

提交信息和扩展信息是[3.4 更新三方组件信息到私人仓库](#update-library)需要使用的。获取方式如下图所示：

| 在gitee网站进入设置界面        |
| ------------------------------ |
| ![image-16](imgs/image-16.png) |
| 进入个人资料获取---姓名        |
| ![image-17](imgs/image-17.png) |
| 进入邮箱管理获取---邮箱        |
| ![image-18](imgs/image-18.png) |





#### 3.4 <a name="update-library"></a>更新三方组件信息到私人仓库

每次更新README之前需要先同步官方推广渠道仓库的代码到我们的third_party_app_libs仓库。这样可以避免PR合入之后导致代码冲突。参照步骤3.2.3点击同步按钮即可。

同步代码操作完成之后先点击我们仓库的README.md文档，然后选择右上方的"编辑"按钮，点击按钮进入编辑模式。

| 点击编辑按钮                   |
| ------------------------------ |
| ![image-04](imgs/image-04.png) |
| 进入编辑模式                   |
| ![image-05](imgs/image-05.png) |



更新内容

| 确认[三方组件准备](#code-prepare) 里的我们的三方组件归属的类型，在相关位置增加三方组件信息。如下所示： |
| ------------------------------------------------------------ |
| ![image-06](imgs/image-06.png)                               |
| 编辑页面的下方添加提交者的信息。<br/>提交信息：填写本次的更新内容。<br/> 扩展信息：添加自己的姓名和邮箱。格式：Signed-off-by: 姓名<邮箱><br/>注意：提交信息和扩展信息必须填写,否则在合入PR的时候无法通过校验<br/>邮箱和姓名需要和gitee账号里面的保持一致，获取方式点击这里[获取提交信息和扩展信息](#get-info) |
| ![image-07](imgs/image-07.png)                               |
| 完成效果                                                     |
| ![image-08](imgs/image-08.png)                               |





## 二 代码PR

我们的third_party_app_libs仓库里面README更新完信息之后，点击项目结构预览上面的第三个按钮Pull Request，创建一个PR将我们的三方组件的信息合入到官方推广渠道的仓库。

### 1.创建PR

| 创建PR                                                       |
| ------------------------------------------------------------ |
| ![image-09](imgs/image-09.png)                               |
| 填入相关信息并提交，请先确认分支是否正确，然后填写本次PR的标题和内容。 |
| ![image-10](imgs/image-10.png)                               |
| 提交完成界面                                                 |
| ![image-11](imgs/image-11.png)                               |

### 2.触发检查

PR创建完成之后还需要在PR的评论里面评论 **sig start build**

| 输入评论                       |
| ------------------------------ |
| ![image-12](imgs/image-12.png) |
| 完成效果                       |
| ![image-13](imgs/image-13.png) |



等待CI流水线启动，对提交的代码进行DCO签署检测，代码质量检测，代码合规问题检测。

| 等待几分钟，刷新[OpenHarmony-SIG/third_party_app_libs](https://gitee.com/openharmony-sig/third_party_app_libs),并点击Pull Request按钮确认CI结果 |
| ------------------------------------------------------------ |
| ![image-14](imgs/image-14.png)                               |
| 点击刚刚提交的PR标题可以查看详情                             |
| ![image-15](imgs/image-15.png)                               |

### 3.修复问题

如果CI检测出现了问题，请按照提示修改，直至修改完所有的问题之后，等待commiter检查代码之后合入该PR。以下列举两种很常见的问题以及其处理方式：

#### 3.1 CI未启动

| 未关联issue，CI未启动                                        |
| ------------------------------------------------------------ |
| ![img-20220713111824](imgs/img-20220713111824.png)           |
| 解决办法：                                                   |
| 点击上图里面的标注1处的issues按钮，新创建一个issue，拿到issue号之后，再回到PR里面点击标注1处关联自己新增的issue，然后再次评论"start build"即可 |



#### 3.2  dco检查失败 

| 未签署DCO或者提交的时候没有添加身份信息                      |
| ------------------------------------------------------------ |
| ![img-20220713112105](imgs/img-20220713112105.png)           |
| 解决办法：                                                   |
| 1.根据提示信息，先确认自己的账号是否签署DCO，具体操作查看[在DCO签署账号](#sign-account)<br/>2.如果账号已经签署了，确认提交的时候是否添加了身份信息，具体操作查看[更新三方组件信息到私人仓库](#update-library)<br/>3.先关闭本次的PR，补全信息之后重新提交一次 |



## 三 更多PR提交方式

本指导文档适用于一些简单的文档、代码修改，不方便进行复杂的操作，如需更详细更专业的操作指导，请参考[贡献流程](https://gitee.com/openharmony/docs/blob/master/zh-cn/contribute/%E8%B4%A1%E7%8C%AE%E6%B5%81%E7%A8%8B.md)

